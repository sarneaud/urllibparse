## `urllibparse`

Direct D translation of [Python's `urllib.parse`](https://docs.python.org/3/library/urllib.parse.html) (beauty spots, warts and all).

### Features

* Supports parsing, manipulation, relative URL joining, quoting, etc
* Supports URLs containing non-UTF-8 character sets
* Reuses Python's test suite based on RFCs and bug reports
* Single-file library with no non-`std` dependencies
* [`@safe`](https://dlang.org/articles/safed.html)

### Usage example

Either add to an existing [DUB project](https://code.dlang.org/) with `dub add urllibparse`, or just add the `urllibparse.d` file to somewhere in your D compiler's import path (such as the main project's source directory).

```d
import urllibparse;

// Parsing
auto parsed = urlParse("https://dlang.org/blog/");
assert (parsed.scheme == "https");
assert (parsed.path == "/blog/");
assert (parsed.password is null);

parsed.path = "documentation.html";
assert (parsed.getUrl() == "https://dlang.org/documentation.html");

// Relative URL handling
assert (urlJoin("https://dlang.org/blog/", "../articles.html") == "https://dlang.org/articles.html");

// Low-level quoting/unquoting
assert (quote("URLs in D!") == "URLs%20in%20D%21");
assert (quotePlus("URLs in D!") == "URLs+in+D%21");
assert (unquote("URLs%20in%20D%21") == "URLs in D!");
assert (unquotePlus("URLs+in+D%21") == "URLs in D!");

// Query encoding
const args = [
	QueryArg("query", "d tools"),
	QueryArg("page", "1"),
	QueryArg("orderBy", "date"),
];
assert (urlEncode(args) == "query=d+tools&page=1&orderBy=date");
// can also use associative arrays, but can't guarantee order or handle repeated keys
assert (urlEncode([
	"query": "d tools",
	"page": "1",
	"orderBy": "date",
]));

// Query parsing
assert (parseQSL("query=d+tools&page=1&orderBy=date") == args);
// likewise, parseQS returns an associative array
```

### Documentation

See [the original Python documentation](https://docs.python.org/3/library/urllib.parse.html) and [the auto-generated D documentation](https://urllibparse.dpldocs.info/).

Differences from Python:
* Function names have been changed to match D's camelCase functionNamingConvention
* `ValueException` is thrown where the Python throws `ValueError`, and `EncodingException` is thrown instead of `UnicodeEncodeError`
* The `ParseResult` class has been changed to a `URLParseResult` struct (and similarly for other `*Result` classes)
* Character encodings (and names) are those supported by [`std.encoding`](https://dlang.org/phobos/std_encoding.html)
* D isn't duck-typey like Python (but pull requests are welcome if you think an overload should be added)
* Some deprecated (trivial) functions are missing.  (They're not in the Python docs now, anyway.)
