module urllibparse;

// This file is a D translation of the following files in the Python 3.9 source:
// * Lib/urllib/parse.py
// * Lib/test/test_urlparse.py
// It's available under the same PSFv2 license.
// https://gitlab.com/sarneaud/urllibparse

@safe:

import std.algorithm;
import std.array;
import std.ascii;
import std.conv;
import std.encoding;
import std.exception;
import std.format;
import std.range;
import std.string;
import std.typecons;
import std.uni;
import std.utf;

// A classification of schemes.
// The empty string classifies URLs with no scheme specified,
// being the default value returned by “urlSplit” and “urlParse”.

immutable uses_relative = [
	"", "ftp", "http", "gopher", "nntp", "imap",
	"wais", "file", "https", "shttp", "mms",
	"prospero", "rtsp", "rtspu", "sftp",
	"svn", "svn+ssh", "ws", "wss"
];

immutable uses_netloc = [
	"", "ftp", "http", "gopher", "nntp", "telnet",
	"imap", "wais", "file", "mms", "https", "shttp",
	"snews", "prospero", "rtsp", "rtspu", "rsync",
	"svn", "svn+ssh", "sftp", "nfs", "git", "git+ssh",
	"ws", "wss"
];

immutable uses_params = [
	"", "ftp", "hdl", "prospero", "http", "imap",
	"https", "shttp", "rtsp", "rtspu", "sip", "sips",
	"mms", "sftp", "tel"
];

// These are not actually used anymore, but should stay for backwards
// compatibility.  (They are undocumented, but have a public-looking name.)

immutable non_hierarchical = [
	"gopher", "hdl", "mailto", "news",
	"telnet", "wais", "imap", "snews", "sip", "sips"
];

immutable uses_query = [
	"", "http", "wais", "imap", "https", "shttp", "mms",
	"gopher", "rtsp", "rtspu", "sip", "sips"
];

immutable uses_fragment = [
	"", "ftp", "hdl", "http", "gopher", "news",
	"nntp", "wais", "https", "shttp", "snews",
	"file", "prospero"
];

// Characters valid in scheme names
immutable scheme_chars = "abcdefghijklmnopqrstuvwxyz" ~ "ABCDEFGHIJKLMNOPQRSTUVWXYZ" ~ "0123456789" ~ "+-.";

/// Result type for urlSplit()
struct URLSplitResult
{
	/// URL component
	string scheme, netloc, path, query, fragment;

	/// Re-construct URL
	string getUrl() const
	{
		return urlUnsplit(this);
	}

	/// Extract username from URL (may return null)
	string username() const
	{
		return _userinfo[0];
	}

	/// Extract password from URL (may return null)
	string password() const
	{
		return _userinfo[1];
	}

	/// Extract hostname from URL (may return null)
	string hostname() const
	{
		auto hostname = _hostinfo[0]; 
		if (hostname.empty) return null;
        // Scoped IPv6 address may have zone info, which must not be lowercased
        // like http://[fe80::822a:a8ff:fe49:470c%tESt]:1234/keys
		if (auto pieces = hostname.byCodeUnit.findSplitBefore("%"))
		{
			return pieces[0].source.toLower() ~ pieces[1].source;
		}
		return hostname.toLower();
	}

	/// Extract port from URL
	Nullable!ushort port() const
	{
		const port_str = _hostinfo[1]; 
		if (port_str is null) return Nullable!ushort.init;
		enforce!ValueException(!port_str.byCodeUnit.any!(c => !std.ascii.isDigit(c)), "Port could not be cast to integer value as '" ~ port_str ~ "'");
		try
		{
			const port = port_str.to!ushort;
			return Nullable!ushort(port);
		}
		catch (Exception e)
		{
			throw new ValueException("Port out of range 0-65535");
		}
	}

	private:

	Tuple!(string, string) _userinfo() const
	{
		const at_idx = netloc.length - 1 - netloc.byCodeUnit.retro.countUntil('@');
		if (at_idx == netloc.length) return Tuple!(string, string)(null, null);
		auto userinfo = netloc.byCodeUnit[0..at_idx];
		if (auto pieces = userinfo.findSplit(":"))
		{
			return tuple(pieces[0].source, pieces[2].source);
		}
		return Tuple!(string, string)(userinfo.source, null);
	}

	Tuple!(string, string) _hostinfo() const
	{
		if (netloc.empty) return Tuple!(string, string)("", null);
		auto hostinfo = netloc.byCodeUnit.splitter('@').tail(1).front;
		string hostname = hostinfo.source, port;
		if (findSkip(hostinfo, "["))
		{
			if (auto close_pieces = hostinfo.findSplit("]"))
			{
				hostname = close_pieces[0].source;
				findSkip(close_pieces[2], ":");
				port = close_pieces[2].source;
			}
		}
		else
		{
			if (auto colon_pieces = hostinfo.findSplit(":"))
			{
				hostname = colon_pieces[0].source;
				findSkip(colon_pieces[2], ":");
				port = colon_pieces[2].source;
			}
		}
		if (port.empty) port = null;
		return tuple(hostname, port);
	}
}

/// Result type for urlParse()
///
/// Based on URLSplitResult and inherits all its members
struct URLParseResult
{
	URLSplitResult split_result;
	alias split_result this;

	/// URL component
	string params;

	this(string scheme, string netloc, string path, string params, string query, string fragment) pure
	{
		this.split_result = URLSplitResult(scheme, netloc, path, query, fragment);
		this.params = params;
	}

	string getUrl() const
	{
		return urlUnparse(this);
	}
}

/**
    Parse a URL into 6 components:
    (scheme)://(netloc)/(path);(params)?(query)#(fragment)

	Return: a 6-tuple: (scheme, netloc, path, params, query, fragment).
    Note that we don't break the components up in smaller bits
    (e.g. netloc is a single string) and we don't expand % escapes.
*/
URLParseResult urlParse(string url, string scheme="", Flag!"allowFragments" allow_fragments=Yes.allowFragments)
{
    auto s = urlSplit(url, scheme, allow_fragments);
	string params;
    if (uses_params.canFind(s.scheme) && s.path.canFind(';'))
	{
		import std.stdio;
		auto params_res = _splitparams(s.path);
		s.path = params_res[0];
		params = params_res[1];
	}
    return URLParseResult(s.scheme, s.netloc, s.path, params, s.query, s.fragment);
}

/**
    Parse a URL into 5 components:
    (scheme)://(netloc)/(path)?(query)#(fragment)

	Return: a 5-tuple: (scheme, netloc, path, query, fragment).
    Note that we don't break the components up in smaller bits
    (e.g. netloc is a single string) and we don't expand % escapes.
*/
URLSplitResult urlSplit(string url, string scheme="", Flag!"allowFragments" allow_fragments=Yes.allowFragments)
{
	auto url_c = url.byCodeUnit;
    string netloc, query, fragment;
	if (auto pieces = url_c.findSplit(":"))
	{
		static bool isSchemeChar(char c) pure
		{
			const ret = std.ascii.isAlphaNum(c) || c.among('+', '-', '.');
			assert (ret == scheme_chars.canFind(c));
			return ret;
		}
		if (pieces[0].all!isSchemeChar)
		{
			scheme = pieces[0].source.toLower();
			url_c = pieces[2];
		}
	}

    if (url_c.startsWith("//"))
	{
		auto netloc_res = _splitnetloc(url_c.source, 2);
		netloc = netloc_res[0];
		url_c = netloc_res[1].byCodeUnit;
		enforce!ValueException(!(netloc.canFind('[') ^ netloc.canFind(']')), "Invalid IPv6 URL");
	}
    if (allow_fragments && url_c.canFind('#'))
	{
		auto pieces = url_c.findSplit("#");
		url_c = pieces[0];
		fragment = pieces[2].source;
	}
    if (url_c.canFind('?'))
	{
		auto pieces = url_c.findSplit("?");
		url_c = pieces[0];
		query = pieces[2].source;
	}
    _checknetloc(netloc);
    return URLSplitResult(scheme, netloc, url_c.source, query, fragment);
}

/**
   Put a parsed URL back together again.  This may result in a
   slightly different, but equivalent URL, if the URL that was parsed
   originally had redundant delimiters, e.g. a ? with an empty query
   (the draft states that these are equivalent).
 */
string urlUnparse(ref const(URLParseResult) components)
{
	URLSplitResult s = components.split_result;
	if (!components.params.empty)
	{
		s.path = s.path ~ ';' ~ components.params;
	}
    return urlUnsplit(s);
}

/**
   Combine the elements of a tuple as returned by urlSplit() into a
   complete URL as a string. The data argument can be any five-item iterable.
   This may result in a slightly different, but equivalent URL, if the URL that
   was parsed originally had unnecessary delimiters (for example, a ? with an
   empty query; the RFC states that these are equivalent).
 */
string urlUnsplit(ref const(URLSplitResult) components)
{
	with (components)
	{
		string ret = path;
		if (!netloc.empty || (!scheme.empty && !path.startsWith("//") && uses_netloc.canFind(scheme)))
		{
			if (!path.empty && !path.startsWith("/")) ret = '/' ~ path;
			ret = "//" ~ netloc ~ ret;
		}
		if (!scheme.empty) ret = scheme ~ ':' ~ ret;
		if (!query.empty) ret = ret ~ '?' ~ query;
		if (!fragment.empty) ret = ret ~ '#' ~ fragment;
		return ret;
	}
}

/**
   Join a base URL and a possibly relative URL to form an absolute
   interpretation of the latter.
 */
string urlJoin(string base, string url, Flag!"allowFragments" allow_fragments=Yes.allowFragments)
{
	if (base.empty) return url;
	if (url.empty) return base;

	auto base_p = urlParse(base, "", allow_fragments);
	auto url_p = urlParse(url, base_p.scheme, allow_fragments);

	if (url_p.scheme != base_p.scheme || !uses_relative.canFind(url_p.scheme))
	{
		return url;
	}

	if (uses_netloc.canFind(url_p.scheme))
	{
		if (!url_p.netloc.empty) return urlUnparse(url_p);
		url_p.netloc = base_p.netloc;
	}

	if (url_p.path.empty && url_p.params.empty)
	{
		url_p.path = base_p.path;
		url_p.params = base_p.params;
		if (url_p.query.empty)
		{
			url_p.query = base_p.query;
		}
		return urlUnparse(url_p);
	}

	auto base_parts = base_p.path.splitter('/');
	if (!base_parts.tail(1).front.empty)
	{
        // the last item is not a directory, so will not be taken into account
        // in resolving the relative path
		base_parts = base_parts.dropBackOne();
	}

    // for rfc3986, ignore all base path should the first character be root.
	string[] segments;
	if (url_p.path.startsWith("/"))
	{
		// NB: urllib doesn't strip redundant slashes in the middle of the URL in this case
		segments = url_p.path.split('/');
	}
	else
	{
        // filter out elements that would cause redundant slashes on re-joining
        // the resolved_path

		segments = chain(base_parts, url_p.path.splitter('/')).enumerate.filter!(t => t[0] == 0 || !t[1].empty).map!(t => t[1]).array;
		if (!segments.endsWith("") && (url_p.path.endsWith("/") || url_p.path.empty)) segments ~= "";
	}

	const is_last_relative = !segments.empty && segments[$-1].among!(".", "..");
	size_t end_idx = 0;
	foreach (seg; segments)
	{
		switch (seg)
		{
			case "..":
				// ignore any .. segments that would otherwise cause an IndexError
				// when popped from resolved_path if resolving for rfc3986
				if (end_idx > 0) --end_idx;
				break;
			case ".":
				break;
			default:
				segments[end_idx++] = seg;
		}
	}
	segments = segments[0..end_idx];
	// do some post-processing here. if the last segment was a relative dir,
	// then we need to append the trailing '/'
	if (is_last_relative) segments ~= "";

	url_p.path = segments.join('/');
	if (url_p.path.empty) url_p.path = "/";
	return urlUnparse(url_p);
}

/// Result type for urlDefrag()
struct URLDefragResult
{
	/// Remainder of URL
	string url;
	/// URL fragment
	string fragment;

	/// Re-construct URL
	string getUrl() const
	{
		if (fragment.empty) return url;
		return url ~ "#" ~ fragment;
	}
}

/**
   Removes any existing fragment from URL.

   Returns a tuple of the defragmented URL and the fragment.  If
   the URL contained no fragments, the second element is the
   empty string.
*/
URLDefragResult urlDefrag(string url)
{
	if (!url.byCodeUnit.canFind("#")) return URLDefragResult(url, "");
	auto parsed = urlParse(url);
	const frag = parsed.fragment;
	parsed.fragment = "";
	const defragged = urlUnparse(parsed);
	return URLDefragResult(defragged, frag);
}

///
immutable(ubyte)[] unquoteToBytes(string input)
{
	// NB: std.uri.decodeComponent requires the result to be valid UTF-8
	auto input_c = input.byCodeUnit;
	if (!input_c.canFind('%')) return input.representation;
	auto items = input_c.splitter('%');
	auto res = appender!(immutable(ubyte)[]);
	res.put(items.front.source.representation);
	items.popFront();
	foreach (item; items)
	{
		if (item.length < 2)
		{
			res.put('%');
			res.put(item.source.representation);
		}
		else
		{
			int value = 0;
			foreach (c; item[0..2])
			{
				value <<= 4;
				switch (c)
				{
					case '0': .. case '9':
							  value += c - '0';
							  break;
					case 'a': .. case 'f':
							  value += 10 + c - 'a';
							  break;
					case 'A': .. case 'F':
							  value += 10 + c - 'A';
							  break;
					default:
							  // force negative value on invalid hex as sentinel
							  value = -255;
				}
			}
			if (value >= 0)
			{
				assert (value < 256);
				res.put(cast(ubyte)value);
				res.put(item[2..$].source.representation);
			}
			else
			{
				res.put('%');
				res.put(item.source.representation);
			}
		}
	}
	return res.data;
}
///
unittest
{
	assert (unquoteToBytes("abc%20def") == "abc def".representation);
}

/// For specifying invalid sequence handling when encoding/decoding
enum EncodingErrorHandling
{
	strict,  /// Raise EncodingException
	replace,  /// Replace bad sequences with placeholder
	ignore,  /// Drop bad sequences
}

/// For compatibility with Python's str.encode
immutable(ubyte)[] encodeString(string s, string encoding, EncodingErrorHandling errors) @trusted
{
	if (s.byCodeUnit.all!isASCII) return s.representation;

	auto ret_app = appender!(immutable(ubyte)[]);
	auto enc = EncodingScheme.create(encoding);

	foreach (dchar d; normalize!NFC(s))
	{
		if (enc.canEncode(d))
		{
			align(4) ubyte[4] buffer;
			auto enc_len = enc.encode(d, buffer[]);
			copy(buffer[0..enc_len], ret_app);
		}
		else
		{
			with (EncodingErrorHandling) final switch (errors)
			{
				case ignore:
					continue;
				case replace:
					copy(enc.replacementSequence, ret_app);
					continue;
				case strict:
					throw new EncodingException(format("Cannot encode string in %s: %s", encoding, s));
			}
		}
	}

	return ret_app.data;
}

/// For compatibility with Python's bytes.decode
string decodeBytes(immutable(ubyte)[] b, string encoding, EncodingErrorHandling errors) @trusted
{
	if (b.all!isASCII) return b.assumeUTF();

	auto ret_app = appender!string;
	auto enc = EncodingScheme.create(encoding);

	auto bc = cast(const(ubyte)[])b;
	while(!bc.empty)
	{
		auto d = enc.safeDecode(bc);
		if (d == INVALID_SEQUENCE)
		{
			with (EncodingErrorHandling) final switch (errors)
			{
				case ignore:
					continue;
				case replace:
					d = replacementDchar;
					break;
				case strict:
					throw new EncodingException(format("Cannot decode data as %s: [%(%02x %)]", encoding, b));
			}
		}
		std.encoding.encode!char(d, ret_app);
	}

	return ret_app.data;
}

/**
   Replace %xx escapes by their single-character equivalent. The optional
   encoding and errors parameters specify how to decode percent-encoded
   sequences into Unicode characters, as accepted by the bytes.decode()
   method.

   By default, percent-encoded sequences are decoded with UTF-8, and invalid
   sequences are replaced by a placeholder character.
*/
string unquote(string input, string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.replace)
{
	if (!input.byCodeUnit.canFind('%'))
	{
		return input;
	}
	// NB: The Python code takes care to only transform contiguous chunks of ASCII.
	// Existing Unicode isn't affected by the encoding.
	auto ret_app = appender!string;
	size_t group_start = 0;
	bool is_group_ascii;
	foreach (j; 0..input.length+1)
	{
		if (j == input.length || isASCII(input[j]) != is_group_ascii)
		{
			auto group = input[group_start..j];
			if (is_group_ascii)
			{
				auto bit = group.unquoteToBytes.decodeBytes(encoding, errors);
				ret_app.put(bit);
			}
			else
			{
				ret_app.put(group);
			}
			group_start = j;
			is_group_ascii = j == input.length || isASCII(input[j]);
		}
	}
	return ret_app.data;
}
///
unittest
{
	assert (unquote("abc%20def") == "abc def");
}

/// For URL query string name=value pairs
struct QueryArg
{
	/// Name part
	string name;
	/// Value part
	string value;
}

/**
    Parse a query given as a string argument.

        Arguments:

        qs: percent-encoded query string to be parsed

        keep_blank_values: flag indicating whether blank values in
            percent-encoded queries should be treated as blank strings.
            A true value indicates that blanks should be retained as
            blank strings.  The default false value indicates that
            blank values are to be ignored and treated as if they were
            not included.

        strict_parsing: flag indicating what to do with parsing errors.
            If false (the default), errors are silently ignored.
            If true, errors raise a ValueError exception.

        encoding and errors: specify how to decode percent-encoded sequences
            into Unicode characters, as accepted by the bytes.decode() method.

        max_num_fields: int. If set, then throws a ValueError if there
            are more than n fields read by parseQSL().

        Returns a dictionary.
*/
string[][string] parseQS(string qs, Flag!"keepBlankValues" keep_blank_values=No.keepBlankValues, Flag!"strictParsing" strict_parsing=No.strictParsing, string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.replace, int max_num_fields=int.max)
{
	auto pairs = parseQSL(qs, keep_blank_values, strict_parsing, encoding, errors, max_num_fields);
	pairs.sort!"a.name < b.name"();
	// NB: chunkBy takes a pointer to the input range, but no pointers escape
	return (() @trusted => pairs.chunkBy!(p => p.name).map!(t => tuple(t[0], t[1].map!(p => p.value).array)).assocArray)();
}

/**
   Parse a query given as a string argument.

	Arguments:

	qs: percent-encoded query string to be parsed

	keep_blank_values: flag indicating whether blank values in
	percent-encoded queries should be treated as blank strings.
	A true value indicates that blanks should be retained as blank
	strings.  The default false value indicates that blank values
	are to be ignored and treated as if they were  not included.

	strict_parsing: flag indicating what to do with parsing errors. If
	false (the default), errors are silently ignored. If true,
	errors raise a ValueError exception.

	encoding and errors: specify how to decode percent-encoded sequences
	into Unicode characters, as accepted by the bytes.decode() method.

	max_num_fields: int. If set, then throws a ValueError
	if there are more than n fields read by parseQSL().

	Returns a list, as G-d intended.
*/
QueryArg[] parseQSL(string qs, Flag!"keepBlankValues" keep_blank_values=No.keepBlankValues, Flag!"strictParsing" strict_parsing=No.strictParsing, string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.replace, int max_num_fields=int.max)
{
	auto qs_c = qs.byCodeUnit;
    // If max_num_fields is defined then check that the number of fields
    // is less than max_num_fields. This prevents a memory exhaustion DOS
    // attack via post bodies [URLs?] with many fields.
	if (max_num_fields != int.max)
	{
		const num_fields = 1 + qs_c.count('&') + qs_c.count(';');
		enforce!ValueException(max_num_fields >= num_fields, "Max number of fields exceeded");
	}

	auto ret = appender!(QueryArg[]);
	foreach(s1; qs_c.splitter('&'))
	{
		foreach(name_value; s1.splitter(';'))
		{
			if (name_value.empty && !strict_parsing) continue;
			string name, value;
			if (auto nv = name_value.findSplit("="))
			{
				name = nv[0].source;
				value = nv[2].source;
			}
			else
			{
				if (strict_parsing)
				{
					throw new ValueException("bad query field: " ~ name_value.source);
				}
				if (keep_blank_values)
				{
					name = name_value.source;
					value = "";
				}
				else
				{
					continue;
				}
			}
			if (!value.empty || keep_blank_values)
			{
				name = name.replace("+", " ");
				name = unquote(name, encoding, errors);
				value = value.replace("+", " ");
				value = unquote(value, encoding, errors);
				ret.put(QueryArg(name, value));
			}
		}
	}
	return ret.data;
}

/**
  Like unquote(), but also replace plus signs by spaces, as required for
  unquoting HTML form values.
 */
string unquotePlus(string input, string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.replace)
{
	input = input.replace("+", " ");
	return unquote(input, encoding, errors);
}
///
unittest
{
	assert (unquotePlus("%7e/abc+def") == "~/abc def");
}

///
string unwrap(string url)
{
	url = url.strip();
	if (url.startsWith("<") && url.endsWith(">"))
	{
		url = url[1..$-1].strip();
	}
	if (url.startsWith("URL:"))
	{
		url = url[4..$].strip();
	}
	return url;
}
///
unittest
{
	assert (unwrap("<URL:type://host/path>") == "type://host/path");
}

/// For customising quote behaviour in some functions
alias string function(string input, const(char)[] safe, string encoding, EncodingErrorHandling errors) QuoteFunction;

/**
    quote('abc def') -> 'abc%20def'

    Each part of a URL, e.g. the path info, the query, etc., has a
    different set of reserved characters that must be quoted.

    RFC 2396 Uniform Resource Identifiers (URI): Generic Syntax lists
    the following reserved characters.

    reserved    = ";" | "/" | "?" | ":" | "@" | "&" | "=" | "+" |
                  "$" | ","

    Each of these characters is reserved in some component of a URL,
    but not necessarily in all of them.

    By default, the quote function is intended for quoting the path
    section of a URL.  Thus, it will not encode '/'.  This character
    is reserved, but in typical usage the quote function is being
    called on a path where the existing slash characters are used as
    reserved characters.

    string and safe may be either str or bytes objects. encoding and errors
    must not be specified if string is a bytes object.

    The optional encoding and errors parameters specify how to deal with
    non-ASCII characters, as accepted by the str.encode method.
    By default, encoding='utf-8' (characters are encoded with UTF-8), and
    errors='strict' (unsupported characters raise a UnicodeEncodeError).
*/
string quote(string input, const(char)[] safe="/", string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.strict)
{
	if (input.empty) return input;
	auto bytes = encodeString(input, encoding, errors);
	return quoteFromBytes(bytes, safe);
}

/**
   Like quote(), but also replace ' ' with '+', as required for quoting
   HTML form values. Plus signs in the original string are escaped unless
   they are included in safe. It also does not have safe default to '/'.
*/
string quotePlus(string input, const(char)[] safe="", string encoding="utf-8", EncodingErrorHandling errors=EncodingErrorHandling.strict)
{
	return quote(input, safe ~ " ", encoding, errors).replace(" ", "+");
}

/*
   Like quote(), but accepts a bytes object rather than a str, and does
   not perform string-to-bytes encoding.  It always returns an ASCII string.
*/
string quoteFromBytes(immutable(ubyte)[] input, const(char)[] safe="/")
{
	if (!safe.all!isASCII)
	{
        // Normalize 'safe' by ... removing non-ASCII chars
		safe = safe.byCodeUnit.filter!isASCII.array;
	}
	bool isSafe(ubyte c)
	{
		return std.ascii.isAlphaNum(c) || c.among('_', '.', '-') || safe.byCodeUnit.canFind(c);
	}
	if (input.all!isSafe) return input.assumeUTF();
	auto ret = appender!string();
	foreach (b; input)
	{
		if (isSafe(b))
		{
			ret.put(cast(immutable(char))b);
		}
		else
		{
			formattedWrite(ret, "%%%02X", b);
		}
	}
	return ret.data;
}
///
unittest
{
	assert (quoteFromBytes("abc def\x3f".representation) == "abc%20def%3F");
}

/**
    Encode a dict or sequence of two-element tuples into a URL query string.

    If any values in the query arg are sequences and doseq is true, each
    sequence element is converted to a separate parameter.

    If the query arg is a sequence of two-element tuples, the order of the
    parameters in the output will match the order of parameters in the
    input.

    The components of a query arg may each be either a string or a bytes type.

    The safe, encoding, and errors parameters are passed down to the function
    specified by quote_via (encoding and errors only if a component is a str).
*/
string urlEncode(const(string[string]) query, const(char)[] safe="", string encoding="utf-8", EncodingErrorHandling errors = EncodingErrorHandling.ignore, QuoteFunction quote_via=&quotePlus)
{
	return urlEncode(query.byPair.map!(p => QueryArg(p[0], p[1])), safe, encoding, errors, quote_via);
}

/// Ditto
string urlEncode(R)(R query, const(char)[] safe="", string encoding="utf-8", EncodingErrorHandling errors = EncodingErrorHandling.ignore, QuoteFunction quote_via=&quotePlus) if (isInputRange!R && is(ElementType!R : const(QueryArg)))
{
	auto quote_pair(QueryArg q)
	{
		return chain(
			quote_via(q.name, safe, encoding, errors).byCodeUnit,
			"=".byCodeUnit,
			quote_via(q.value, safe, encoding, errors).byCodeUnit
		);
	}
	return query.map!quote_pair.join('&');
}

/// D alternative to Python's ValueError
class ValueException : Exception
{
	mixin basicExceptionCtors;
}

private:

Tuple!(string, string) _splitparams(string url)
{
	auto after_slashes = url.byCodeUnit.splitter('/').tail(1).front;
	const semicolon_offset = after_slashes.countUntil(';');
	if (semicolon_offset == -1) return tuple(url, "");
	const semicolon_idx = url.length - after_slashes.length + semicolon_offset;
	return tuple(url[0..semicolon_idx], url[semicolon_idx+1..$]);
}

Tuple!(string, string) _splitnetloc(string url, size_t start=0)
{
	// position of end of domain part of url, default is end
	auto delim_idx = start + url[start..$].byCodeUnit.countUntil!(c => !!c.among('/', '?', '#'));
	if (delim_idx < start) delim_idx = url.length; // countUntil returns -1 on miss
    return tuple(url[start..delim_idx], url[delim_idx..$]); // return (domain, rest)
}

void _checknetloc(string netloc)
{
    if (netloc.all!isASCII) return;
    // looking for characters like \u2100 that expand to 'a/c'
    // IDNA uses NFKC equivalence, so normalize for this check

	// ignore characters already included
	// but not the surrounding text
    auto n = netloc.dup.remove!(c => !!c.among('@', ':', '#', '?'));
    const netloc2 = n.normalize!NFKC();
    if (n == netloc2) return;
	enforce!ValueException(!netloc2.canFind!(c => c.among('/', '?', '#', '@', ':')), "netloc '" ~ netloc ~ "' contains invalid characters under NFKC normalization");
}

version (unittest)
{
	immutable RFC1808_BASE = "http://a/b/c/d;p?q#f";
	immutable RFC2396_BASE = "http://a/b/c/d;p?q";
	immutable RFC3986_BASE = "http://a/b/c/d;p?q";
	immutable SIMPLE_BASE  = "http://a/b/c/d";

	void assertEqual(V)(auto ref V result, auto ref V expected, lazy string msg = "Error")
	{
		import std.traits : isAssociativeArray;
		static if (isForwardRange!V)
		{
			const pass = equal(result, expected);
		}
		else static if (isAssociativeArray!V)
		{
			auto result_pairs = result.byPair.array.sort;
			auto expected_pairs = expected.byPair.array.sort;
			const pass = equal(result_pairs, expected_pairs);
		}
		else
		{
			const pass = result == expected;
		}
		assert (pass, format("%s: result %s != expected %s\n", msg, result, expected));
	}
}

// Usage example
unittest
{
	// Parsing
	auto parsed = urlParse("https://dlang.org/blog/");
	assert (parsed.scheme == "https");
	assert (parsed.path == "/blog/");
	assert (parsed.password is null);

	parsed.path = "documentation.html";
	assert (parsed.getUrl() == "https://dlang.org/documentation.html");

	// Relative URL handling
	assert (urlJoin("https://dlang.org/blog/", "../articles.html") == "https://dlang.org/articles.html");

	// Low-level quoting/unquoting
	assert (quote("URLs in D!") == "URLs%20in%20D%21");
	assert (quotePlus("URLs in D!") == "URLs+in+D%21");
	assert (unquote("URLs%20in%20D%21") == "URLs in D!");
	assert (unquotePlus("URLs+in+D%21") == "URLs in D!");

	// Query encoding
	const args = [
		QueryArg("query", "d tools"),
		QueryArg("page", "1"),
		QueryArg("orderBy", "date"),
	];
	assert (urlEncode(args) == "query=d+tools&page=1&orderBy=date");
	// can also use associative arrays, but can't guarantee order or handle repeated keys
	assert (urlEncode([
				"query": "d tools",
				"page": "1",
				"orderBy": "date",
	]));

	// Query parsing
	assert (parseQSL("query=d+tools&page=1&orderBy=date") == args);
	// likewise, parseQS returns an associative array
}

unittest
{
	// test_qsl

	static struct TestCase
	{
		string qs;
		QueryArg[] expected;
	}

	auto parseQSL_test_cases = [
		TestCase("", []),
		TestCase("&", []),
		TestCase("&&", []),
		TestCase("=", [QueryArg("", "")]),
		TestCase("=a", [QueryArg("", "a")]),
		TestCase("a", [QueryArg("a", "")]),
		TestCase("a=", [QueryArg("a", "")]),
		TestCase("&a=b", [QueryArg("a", "b")]),
		TestCase("a=a+b&b=b+c", [QueryArg("a", "a b"), QueryArg("b", "b c")]),
		TestCase("a=1&a=2", [QueryArg("a", "1"), QueryArg("a", "2")]),
		TestCase("", []),
		TestCase("&", []),
		TestCase("&&", []),
		TestCase("=", [QueryArg("", "")]),
		TestCase("=a", [QueryArg("", "a")]),
		TestCase("a", [QueryArg("a", "")]),
		TestCase("a=", [QueryArg("a", "")]),
		TestCase("&a=b", [QueryArg("a", "b")]),
		TestCase("a=a+b&b=b+c", [QueryArg("a", "a b"), QueryArg("b", "b c")]),
		TestCase("a=1&a=2", [QueryArg("a", "1"), QueryArg("a", "2")]),
		TestCase(";", []),
		TestCase(";;", []),
		TestCase(";a=b", [QueryArg("a", "b")]),
		TestCase("a=a+b;b=b+c", [QueryArg("a", "a b"), QueryArg("b", "b c")]),
		TestCase("a=1;a=2", [QueryArg("a", "1"), QueryArg("a", "2")]),
		TestCase(";", []),
		TestCase(";;", []),
		TestCase(";a=b", [QueryArg("a", "b")]),
		TestCase("a=a+b;b=b+c", [QueryArg("a", "a b"), QueryArg("b", "b c")]),
		TestCase("a=1;a=2", [QueryArg("a", "1"), QueryArg("a", "2")]),
	];

	foreach (uat; parseQSL_test_cases)
	{
		auto result = parseQSL(uat.qs, Yes.keepBlankValues); 
		assertEqual(result, uat.expected, "Error parsing " ~ uat.qs);
		auto expected_without_blanks = uat.expected.filter!(v => !v.value.empty).array;
		result = parseQSL(uat.qs, No.keepBlankValues); 
		assertEqual(result, expected_without_blanks, "Error parsing " ~ uat.qs);
	}
}

unittest
{
	// test_qs

	static struct TestCase
	{
		string qs;
		string[][string] expected;
	}

	auto parseQS_test_cases = [
		TestCase("", string[][string].init),
		TestCase("&", string[][string].init),
		TestCase("&&", string[][string].init),
		TestCase("=", ["": [""]]),
		TestCase("=a", ["": ["a"]]),
		TestCase("a", ["a": [""]]),
		TestCase("a=", ["a": [""]]),
		TestCase("&a=b", ["a": ["b"]]),
		TestCase("a=a+b&b=b+c", ["a": ["a b"], "b": ["b c"]]),
		TestCase("a=1&a=2", ["a": ["1", "2"]]),
		TestCase("", string[][string].init),
		TestCase("&", string[][string].init),
		TestCase("&&", string[][string].init),
		TestCase("=", ["": [""]]),
		TestCase("=a", ["": ["a"]]),
		TestCase("a", ["a": [""]]),
		TestCase("a=", ["a": [""]]),
		TestCase("&a=b", ["a": ["b"]]),
		TestCase("a=a+b&b=b+c", ["a": ["a b"], "b": ["b c"]]),
		TestCase("a=1&a=2", ["a": ["1", "2"]]),
		TestCase(";", string[][string].init),
		TestCase(";;", string[][string].init),
		TestCase(";a=b", ["a": ["b"]]),
		TestCase("a=a+b;b=b+c", ["a": ["a b"], "b": ["b c"]]),
		TestCase("a=1;a=2", ["a": ["1", "2"]]),
		TestCase(";", string[][string].init),
		TestCase(";;", string[][string].init),
		TestCase(";a=b", ["a": ["b"]]),
		TestCase("a=a+b;b=b+c", ["a": ["a b"], "b": ["b c"]]),
		TestCase("a=1;a=2", ["a": ["1", "2"]]),
	];

	foreach (uat; parseQS_test_cases)
	{
		auto result = parseQS(uat.qs, Yes.keepBlankValues); 
		assertEqual(result, uat.expected, "Error parsing " ~ uat.qs);
		auto expected_without_blanks = uat.expected.dup();
		foreach (item; expected_without_blanks.byKeyValue)
		{
			if (item.value[0].empty) expected_without_blanks.remove(item.key);
		}
		result = parseQS(uat.qs, No.keepBlankValues); 
		assertEqual(result, expected_without_blanks, "Error parsing " ~ uat.qs);
	}
}

unittest
{
	// test_roundtrips

    void checkRoundtrips(string url, ref URLParseResult parsed, ref URLSplitResult split)
	{
		{
			auto result = urlParse(url);
			assertEqual(result, parsed);
			// put it back together and it should be the same
			auto result2 = urlUnparse(result);
			assertEqual(result2, url);
			assertEqual(result2, result.getUrl());

			// the result of getUrl() is a fixpoint; we can always parse it
			// again to get the same result:
			auto result3 = urlParse(result.getUrl());
			assertEqual(result3.getUrl(), result.getUrl());
			assertEqual(result3,          result);
			assertEqual(result3.scheme,   result.scheme);
			assertEqual(result3.netloc,   result.netloc);
			assertEqual(result3.path,     result.path);
			assertEqual(result3.params,   result.params);
			assertEqual(result3.query,    result.query);
			assertEqual(result3.fragment, result.fragment);
			assertEqual(result3.username, result.username);
			assertEqual(result3.password, result.password);
			assertEqual(result3.hostname, result.hostname);
			assertEqual(result3.port,     result.port);
		}

		{
			// check the roundtrip using urlSplit() as well
			auto result = urlSplit(url);
			assertEqual(result, split);
			auto result2 = urlUnsplit(result);
			assertEqual(result2, url);
			assertEqual(result2, result.getUrl());

			// check the fixpoint property of re-parsing the result of getUrl()
			auto result3 = urlSplit(result.getUrl());
			assertEqual(result3.getUrl(), result.getUrl());
			assertEqual(result3,          result);
			assertEqual(result3.scheme,   result.scheme);
			assertEqual(result3.netloc,   result.netloc);
			assertEqual(result3.path,     result.path);
			assertEqual(result3.query,    result.query);
			assertEqual(result3.fragment, result.fragment);
			assertEqual(result3.username, result.username);
			assertEqual(result3.password, result.password);
			assertEqual(result3.hostname, result.hostname);
			assertEqual(result3.port,     result.port);
		}
	}

	static struct ParseSplitCase
	{
		string url;
		URLParseResult parsed;
		URLSplitResult split;
	}

	// test_roundtrips
	{
		auto test_cases = [
			ParseSplitCase("file:///tmp/junk.txt",
				URLParseResult("file", "", "/tmp/junk.txt", "", "", ""),
				URLSplitResult("file", "", "/tmp/junk.txt", "", "")),
			ParseSplitCase("imap://mail.python.org/mbox1",
				URLParseResult("imap", "mail.python.org", "/mbox1", "", "", ""),
				URLSplitResult("imap", "mail.python.org", "/mbox1", "", "")),
			ParseSplitCase("mms://wms.sys.hinet.net/cts/Drama/09006251100.asf",
				URLParseResult("mms", "wms.sys.hinet.net", "/cts/Drama/09006251100.asf", "", "", ""),
				URLSplitResult("mms", "wms.sys.hinet.net", "/cts/Drama/09006251100.asf", "", "")),
			ParseSplitCase("nfs://server/path/to/file.txt",
				URLParseResult("nfs", "server", "/path/to/file.txt", "", "", ""),
				URLSplitResult("nfs", "server", "/path/to/file.txt", "", "")),
			ParseSplitCase("svn+ssh://svn.zope.org/repos/main/ZConfig/trunk/",
				URLParseResult("svn+ssh", "svn.zope.org", "/repos/main/ZConfig/trunk/", "", "", ""),
				URLSplitResult("svn+ssh", "svn.zope.org", "/repos/main/ZConfig/trunk/", "", "")),
			ParseSplitCase("git+ssh://git@github.com/user/project.git",
				URLParseResult("git+ssh", "git@github.com","/user/project.git", "","",""),
				URLSplitResult("git+ssh", "git@github.com","/user/project.git", "", "")),
		];
		foreach (uat; test_cases)
		{
			checkRoundtrips(uat.url, uat.parsed, uat.split);
		}
	}

	// test_http_roundtrips
	{
        // urllib.parse.urlSplit treats 'http:' as an optimized special case,
        // so we test both 'http:' and 'https:' in all the following.
        // Three cheers for white box knowledge!
		// NB: The above was true in Python 3.8 but the special case got removed
		auto test_cases = [
            ParseSplitCase("://www.python.org",
				URLParseResult("", "www.python.org", "", "", "", ""),
				URLSplitResult("", "www.python.org", "", "", "")),
            ParseSplitCase("://www.python.org#abc",
				URLParseResult("", "www.python.org", "", "", "", "abc"),
				URLSplitResult("", "www.python.org", "", "", "abc")),
            ParseSplitCase("://www.python.org?q=abc",
				URLParseResult("", "www.python.org", "", "", "q=abc", ""),
				URLSplitResult("", "www.python.org", "", "q=abc", "")),
            ParseSplitCase("://www.python.org/#abc",
				URLParseResult("", "www.python.org", "/", "", "", "abc"),
				URLSplitResult("", "www.python.org", "/", "", "abc")),
            ParseSplitCase("://a/b/c/d;p?q#f",
				URLParseResult("", "a", "/b/c/d", "p", "q", "f"),
				URLSplitResult("", "a", "/b/c/d;p", "q", "f")),
		];
		foreach (uat; test_cases)
		{
			auto parsed = uat.parsed;
			auto split = uat.split;
			parsed.scheme = split.scheme = "http";
			checkRoundtrips("http" ~ uat.url, parsed, split);
			parsed.scheme = split.scheme = "https";
			checkRoundtrips("https" ~ uat.url, parsed, split);
		}
	}
}

unittest
{
	// test_unparse_parse
	auto test_cases = ["Python", "./Python","x-newscheme://foo.com/stuff","x://y","x:/y","x:/","/",];
	foreach (uat; test_cases)
	{
		auto split = urlSplit(uat);
		assertEqual(urlUnsplit(split), uat, "Error splitting");
		auto parsed = urlParse(uat);
		assertEqual(urlUnparse(parsed), uat, "Error parsing");
	}
}

unittest
{
	void checkJoin(string base, string relurl, string expected)
	{
		assertEqual(urlJoin(base, relurl), expected, "Error joining " ~ relurl ~ " to base " ~ base);
	}

	// testRFC1808 
	// "normal" cases from RFC 1808:
	checkJoin(RFC1808_BASE, "g:h", "g:h");
	checkJoin(RFC1808_BASE, "g", "http://a/b/c/g");
	checkJoin(RFC1808_BASE, "./g", "http://a/b/c/g");
	checkJoin(RFC1808_BASE, "g/", "http://a/b/c/g/");
	checkJoin(RFC1808_BASE, "/g", "http://a/g");
	checkJoin(RFC1808_BASE, "//g", "http://g");
	checkJoin(RFC1808_BASE, "g?y", "http://a/b/c/g?y");
	checkJoin(RFC1808_BASE, "g?y/./x", "http://a/b/c/g?y/./x");
	checkJoin(RFC1808_BASE, "#s", "http://a/b/c/d;p?q#s");
	checkJoin(RFC1808_BASE, "g#s", "http://a/b/c/g#s");
	checkJoin(RFC1808_BASE, "g#s/./x", "http://a/b/c/g#s/./x");
	checkJoin(RFC1808_BASE, "g?y#s", "http://a/b/c/g?y#s");
	checkJoin(RFC1808_BASE, "g;x", "http://a/b/c/g;x");
	checkJoin(RFC1808_BASE, "g;x?y#s", "http://a/b/c/g;x?y#s");
	checkJoin(RFC1808_BASE, ".", "http://a/b/c/");
	checkJoin(RFC1808_BASE, "./", "http://a/b/c/");
	checkJoin(RFC1808_BASE, "..", "http://a/b/");
	checkJoin(RFC1808_BASE, "../", "http://a/b/");
	checkJoin(RFC1808_BASE, "../g", "http://a/b/g");
	checkJoin(RFC1808_BASE, "../..", "http://a/");
	checkJoin(RFC1808_BASE, "../../", "http://a/");
	checkJoin(RFC1808_BASE, "../../g", "http://a/g");

	// "abnormal" cases from RFC 1808:
	checkJoin(RFC1808_BASE, "", "http://a/b/c/d;p?q#f");
	checkJoin(RFC1808_BASE, "g.", "http://a/b/c/g.");
	checkJoin(RFC1808_BASE, ".g", "http://a/b/c/.g");
	checkJoin(RFC1808_BASE, "g..", "http://a/b/c/g..");
	checkJoin(RFC1808_BASE, "..g", "http://a/b/c/..g");
	checkJoin(RFC1808_BASE, "./../g", "http://a/b/g");
	checkJoin(RFC1808_BASE, "./g/.", "http://a/b/c/g/");
	checkJoin(RFC1808_BASE, "g/./h", "http://a/b/c/g/h");
	checkJoin(RFC1808_BASE, "g/../h", "http://a/b/c/h");

	/*
	# RFC 1808 and RFC 1630 disagree on these (according to RFC 1808),
	# so we"ll not actually run these tests (which expect 1808 behavior).
	checkJoin(RFC1808_BASE, "http:g", "http:g");
	checkJoin(RFC1808_BASE, "http:", "http:");

	# XXX: The following tests are no longer compatible with RFC3986
	checkJoin(RFC1808_BASE, "../../../g", "http://a/../g");
	checkJoin(RFC1808_BASE, "../../../../g", "http://a/../../g");
	checkJoin(RFC1808_BASE, "/./g", "http://a/./g");
	checkJoin(RFC1808_BASE, "/../g", "http://a/../g");
	 */

	// test_RFC2368
	// Issue 11467: path that starts with a number is not parsed correctly
	assertEqual(urlParse("mailto:1337@example.org"), URLParseResult("mailto", "", "1337@example.org", "", "", ""));

	// test_RFC2396
	// cases from RFC 2396

	checkJoin(RFC2396_BASE, "g:h", "g:h");
	checkJoin(RFC2396_BASE, "g", "http://a/b/c/g");
	checkJoin(RFC2396_BASE, "./g", "http://a/b/c/g");
	checkJoin(RFC2396_BASE, "g/", "http://a/b/c/g/");
	checkJoin(RFC2396_BASE, "/g", "http://a/g");
	checkJoin(RFC2396_BASE, "//g", "http://g");
	checkJoin(RFC2396_BASE, "g?y", "http://a/b/c/g?y");
	checkJoin(RFC2396_BASE, "#s", "http://a/b/c/d;p?q#s");
	checkJoin(RFC2396_BASE, "g#s", "http://a/b/c/g#s");
	checkJoin(RFC2396_BASE, "g?y#s", "http://a/b/c/g?y#s");
	checkJoin(RFC2396_BASE, "g;x", "http://a/b/c/g;x");
	checkJoin(RFC2396_BASE, "g;x?y#s", "http://a/b/c/g;x?y#s");
	checkJoin(RFC2396_BASE, ".", "http://a/b/c/");
	checkJoin(RFC2396_BASE, "./", "http://a/b/c/");
	checkJoin(RFC2396_BASE, "..", "http://a/b/");
	checkJoin(RFC2396_BASE, "../", "http://a/b/");
	checkJoin(RFC2396_BASE, "../g", "http://a/b/g");
	checkJoin(RFC2396_BASE, "../..", "http://a/");
	checkJoin(RFC2396_BASE, "../../", "http://a/");
	checkJoin(RFC2396_BASE, "../../g", "http://a/g");
	checkJoin(RFC2396_BASE, "", RFC2396_BASE);
	checkJoin(RFC2396_BASE, "g.", "http://a/b/c/g.");
	checkJoin(RFC2396_BASE, ".g", "http://a/b/c/.g");
	checkJoin(RFC2396_BASE, "g..", "http://a/b/c/g..");
	checkJoin(RFC2396_BASE, "..g", "http://a/b/c/..g");
	checkJoin(RFC2396_BASE, "./../g", "http://a/b/g");
	checkJoin(RFC2396_BASE, "./g/.", "http://a/b/c/g/");
	checkJoin(RFC2396_BASE, "g/./h", "http://a/b/c/g/h");
	checkJoin(RFC2396_BASE, "g/../h", "http://a/b/c/h");
	checkJoin(RFC2396_BASE, "g;x=1/./y", "http://a/b/c/g;x=1/y");
	checkJoin(RFC2396_BASE, "g;x=1/../y", "http://a/b/c/y");
	checkJoin(RFC2396_BASE, "g?y/./x", "http://a/b/c/g?y/./x");
	checkJoin(RFC2396_BASE, "g?y/../x", "http://a/b/c/g?y/../x");
	checkJoin(RFC2396_BASE, "g#s/./x", "http://a/b/c/g#s/./x");
	checkJoin(RFC2396_BASE, "g#s/../x", "http://a/b/c/g#s/../x");

	/*
	# XXX: The following tests are no longer compatible with RFC3986
	checkJoin(RFC2396_BASE, "../../../g", "http://a/../g");
	checkJoin(RFC2396_BASE, "../../../../g", "http://a/../../g");
	checkJoin(RFC2396_BASE, "/./g", "http://a/./g");
	checkJoin(RFC2396_BASE, "/../g", "http://a/../g");
	 */

	// test_RFC3986
	checkJoin(RFC3986_BASE, "?y","http://a/b/c/d;p?y");
	checkJoin(RFC3986_BASE, ";x", "http://a/b/c/;x");
	checkJoin(RFC3986_BASE, "g:h","g:h");
	checkJoin(RFC3986_BASE, "g","http://a/b/c/g");
	checkJoin(RFC3986_BASE, "./g","http://a/b/c/g");
	checkJoin(RFC3986_BASE, "g/","http://a/b/c/g/");
	checkJoin(RFC3986_BASE, "/g","http://a/g");
	checkJoin(RFC3986_BASE, "//g","http://g");
	checkJoin(RFC3986_BASE, "?y","http://a/b/c/d;p?y");
	checkJoin(RFC3986_BASE, "g?y","http://a/b/c/g?y");
	checkJoin(RFC3986_BASE, "#s","http://a/b/c/d;p?q#s");
	checkJoin(RFC3986_BASE, "g#s","http://a/b/c/g#s");
	checkJoin(RFC3986_BASE, "g?y#s","http://a/b/c/g?y#s");
	checkJoin(RFC3986_BASE, ";x","http://a/b/c/;x");
	checkJoin(RFC3986_BASE, "g;x","http://a/b/c/g;x");
	checkJoin(RFC3986_BASE, "g;x?y#s","http://a/b/c/g;x?y#s");
	checkJoin(RFC3986_BASE, "","http://a/b/c/d;p?q");
	checkJoin(RFC3986_BASE, ".","http://a/b/c/");
	checkJoin(RFC3986_BASE, "./","http://a/b/c/");
	checkJoin(RFC3986_BASE, "..","http://a/b/");
	checkJoin(RFC3986_BASE, "../","http://a/b/");
	checkJoin(RFC3986_BASE, "../g","http://a/b/g");
	checkJoin(RFC3986_BASE, "../..","http://a/");
	checkJoin(RFC3986_BASE, "../../","http://a/");
	checkJoin(RFC3986_BASE, "../../g","http://a/g");
	checkJoin(RFC3986_BASE, "../../../g", "http://a/g");

	// Abnormal Examples

	// The 'abnormal scenarios' are incompatible with RFC2986 parsing
	// Tests are here for reference.

	checkJoin(RFC3986_BASE, "../../../g","http://a/g");
	checkJoin(RFC3986_BASE, "../../../../g","http://a/g");
	checkJoin(RFC3986_BASE, "/./g","http://a/g");
	checkJoin(RFC3986_BASE, "/../g","http://a/g");
	checkJoin(RFC3986_BASE, "g.","http://a/b/c/g.");
	checkJoin(RFC3986_BASE, ".g","http://a/b/c/.g");
	checkJoin(RFC3986_BASE, "g..","http://a/b/c/g..");
	checkJoin(RFC3986_BASE, "..g","http://a/b/c/..g");
	checkJoin(RFC3986_BASE, "./../g","http://a/b/g");
	checkJoin(RFC3986_BASE, "./g/.","http://a/b/c/g/");
	checkJoin(RFC3986_BASE, "g/./h","http://a/b/c/g/h");
	checkJoin(RFC3986_BASE, "g/../h","http://a/b/c/h");
	checkJoin(RFC3986_BASE, "g;x=1/./y","http://a/b/c/g;x=1/y");
	checkJoin(RFC3986_BASE, "g;x=1/../y","http://a/b/c/y");
	checkJoin(RFC3986_BASE, "g?y/./x","http://a/b/c/g?y/./x");
	checkJoin(RFC3986_BASE, "g?y/../x","http://a/b/c/g?y/../x");
	checkJoin(RFC3986_BASE, "g#s/./x","http://a/b/c/g#s/./x");
	checkJoin(RFC3986_BASE, "g#s/../x","http://a/b/c/g#s/../x");
	// checkJoin(RFC3986_BASE, "http:g","http:g"); // strict parser
	checkJoin(RFC3986_BASE, "http:g","http://a/b/c/g"); // relaxed parser

	// Test for issue9721
	checkJoin("http://a/b/c/de", ";x","http://a/b/c/;x");

	// test_urljoins
	checkJoin(SIMPLE_BASE, "g:h","g:h");
	checkJoin(SIMPLE_BASE, "http:g","http://a/b/c/g");
	checkJoin(SIMPLE_BASE, "http:","http://a/b/c/d");
	checkJoin(SIMPLE_BASE, "g","http://a/b/c/g");
	checkJoin(SIMPLE_BASE, "./g","http://a/b/c/g");
	checkJoin(SIMPLE_BASE, "g/","http://a/b/c/g/");
	checkJoin(SIMPLE_BASE, "/g","http://a/g");
	checkJoin(SIMPLE_BASE, "//g","http://g");
	checkJoin(SIMPLE_BASE, "?y","http://a/b/c/d?y");
	checkJoin(SIMPLE_BASE, "g?y","http://a/b/c/g?y");
	checkJoin(SIMPLE_BASE, "g?y/./x","http://a/b/c/g?y/./x");
	checkJoin(SIMPLE_BASE, ".","http://a/b/c/");
	checkJoin(SIMPLE_BASE, "./","http://a/b/c/");
	checkJoin(SIMPLE_BASE, "..","http://a/b/");
	checkJoin(SIMPLE_BASE, "../","http://a/b/");
	checkJoin(SIMPLE_BASE, "../g","http://a/b/g");
	checkJoin(SIMPLE_BASE, "../..","http://a/");
	checkJoin(SIMPLE_BASE, "../../g","http://a/g");
	checkJoin(SIMPLE_BASE, "./../g","http://a/b/g");
	checkJoin(SIMPLE_BASE, "./g/.","http://a/b/c/g/");
	checkJoin(SIMPLE_BASE, "g/./h","http://a/b/c/g/h");
	checkJoin(SIMPLE_BASE, "g/../h","http://a/b/c/h");
	checkJoin(SIMPLE_BASE, "http:g","http://a/b/c/g");
	checkJoin(SIMPLE_BASE, "http:","http://a/b/c/d");
	checkJoin(SIMPLE_BASE, "http:?y","http://a/b/c/d?y");
	checkJoin(SIMPLE_BASE, "http:g?y","http://a/b/c/g?y");
	checkJoin(SIMPLE_BASE, "http:g?y/./x","http://a/b/c/g?y/./x");
	checkJoin("http:///", "..","http:///");
	checkJoin("", "http://a/b/c/g?y/./x","http://a/b/c/g?y/./x");
	checkJoin("", "http://a/./g", "http://a/./g");
	checkJoin("svn://pathtorepo/dir1", "dir2", "svn://pathtorepo/dir2");
	checkJoin("svn+ssh://pathtorepo/dir1", "dir2", "svn+ssh://pathtorepo/dir2");
	checkJoin("ws://a/b","g","ws://a/g");
	checkJoin("wss://a/b","g","wss://a/g");

	/*
	# XXX: The following tests are no longer compatible with RFC3986
	checkJoin(SIMPLE_BASE, "../../../g","http://a/../g");
	checkJoin(SIMPLE_BASE, "/./g","http://a/./g");
	 */

	// test for issue22118 duplicate slashes
	checkJoin(SIMPLE_BASE ~ "/", "foo", SIMPLE_BASE ~ "/foo");

	// Non-RFC-defined tests, covering variations of base and trailing
	// slashes
	checkJoin("http://a/b/c/d/e/", "../../f/g/", "http://a/b/c/f/g/");
	checkJoin("http://a/b/c/d/e", "../../f/g/", "http://a/b/f/g/");
	checkJoin("http://a/b/c/d/e/", "/../../f/g/", "http://a/f/g/");
	checkJoin("http://a/b/c/d/e", "/../../f/g/", "http://a/f/g/");
	checkJoin("http://a/b/c/d/e/", "../../f/g", "http://a/b/c/f/g");
	checkJoin("http://a/b/", "../../f/g/", "http://a/f/g/");

	// issue 23703: don't duplicate filename
	checkJoin("a", "b", "b");
}

unittest
{
	// test_RFC2732
	static struct TestCase
	{
		string url, hostname;
		Nullable!ushort port;

		this(string url, string hostname, ushort port)
		{
			this.url = url;
			this.hostname = hostname;
			this.port = Nullable!ushort(port);
		}

		this(string url, string hostname)
		{
			this.url = url;
			this.hostname = hostname;
		}
	}
	auto test_cases = [
		TestCase("http://Test.python.org:5432/foo/", "test.python.org", 5432),
		TestCase("http://12.34.56.78:5432/foo/", "12.34.56.78", 5432),
		TestCase("http://[::1]:5432/foo/", "::1", 5432),
		TestCase("http://[dead:beef::1]:5432/foo/", "dead:beef::1", 5432),
		TestCase("http://[dead:beef::]:5432/foo/", "dead:beef::", 5432),
		TestCase("http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]:5432/foo/",
				"dead:beef:cafe:5417:affe:8fa3:deaf:feed", 5432),
		TestCase("http://[::12.34.56.78]:5432/foo/", "::12.34.56.78", 5432),
		TestCase("http://[::ffff:12.34.56.78]:5432/foo/",
				"::ffff:12.34.56.78", 5432),
		TestCase("http://Test.python.org/foo/", "test.python.org"),
		TestCase("http://12.34.56.78/foo/", "12.34.56.78"),
		TestCase("http://[::1]/foo/", "::1"),
		TestCase("http://[dead:beef::1]/foo/", "dead:beef::1"),
		TestCase("http://[dead:beef::]/foo/", "dead:beef::"),
		TestCase("http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]/foo/",
				"dead:beef:cafe:5417:affe:8fa3:deaf:feed"),
		TestCase("http://[::12.34.56.78]/foo/", "::12.34.56.78"),
		TestCase("http://[::ffff:12.34.56.78]/foo/",
				"::ffff:12.34.56.78"),
		TestCase("http://Test.python.org:/foo/", "test.python.org"),
		TestCase("http://12.34.56.78:/foo/", "12.34.56.78"),
		TestCase("http://[::1]:/foo/", "::1"),
		TestCase("http://[dead:beef::1]:/foo/", "dead:beef::1"),
		TestCase("http://[dead:beef::]:/foo/", "dead:beef::"),
		TestCase("http://[dead:beef:cafe:5417:affe:8FA3:deaf:feed]:/foo/",
				"dead:beef:cafe:5417:affe:8fa3:deaf:feed"),
		TestCase("http://[::12.34.56.78]:/foo/", "::12.34.56.78"),
		TestCase("http://[::ffff:12.34.56.78]:/foo/",
				"::ffff:12.34.56.78"),
	];
	foreach (uat; test_cases)
	{
		auto urlParsed = urlParse(uat.url);
		assertEqual(urlParsed.hostname, uat.hostname, "Error parsing " ~ uat.url);
		assertEqual(urlParsed.port, uat.port, "Error parsing " ~ uat.url);
	}

	auto bad_cases = [
		"http://::12.34.56.78]/",
		"http://[::1/foo/",
		"ftp://[::1/foo/bad]/bad",
		"http://[::1/foo/bad]/bad",
		"http://[::ffff:12.34.56.78"];
	foreach(uat; bad_cases)
	{
		assertThrown(urlParse(uat), "Invalid URL " ~ uat ~ " didn't generate error");
	}
}

unittest
{
	// test_urldefrag
	static struct TestCase
	{
		string url, defrag, frag;
	}
	auto test_cases = [
		TestCase("http://python.org#frag", "http://python.org", "frag"),
		TestCase("http://python.org", "http://python.org", ""),
		TestCase("http://python.org/#frag", "http://python.org/", "frag"),
		TestCase("http://python.org/", "http://python.org/", ""),
		TestCase("http://python.org/?q#frag", "http://python.org/?q", "frag"),
		TestCase("http://python.org/?q", "http://python.org/?q", ""),
		TestCase("http://python.org/p#frag", "http://python.org/p", "frag"),
		TestCase("http://python.org/p?q", "http://python.org/p?q", ""),
		TestCase(RFC1808_BASE, "http://a/b/c/d;p?q", "f"),
		TestCase(RFC2396_BASE, "http://a/b/c/d;p?q", ""),
	];
	foreach (uat; test_cases)
	{
		auto result = urlDefrag(uat.url);
		assertEqual(result.url, uat.defrag);
		assertEqual(result.fragment, uat.frag);
		assertEqual(result.getUrl(), uat.url);
	}
}

unittest
{
	// test_urlsplit_scoped_IPv6
	auto p = urlSplit("http://[FE80::822a:a8ff:fe49:470c%tESt]:1234");
	assertEqual(p.hostname, "fe80::822a:a8ff:fe49:470c%tESt");
	assertEqual(p.netloc, "[FE80::822a:a8ff:fe49:470c%tESt]:1234");
}

unittest
{
	// test_urlsplit_attributes
	string url = "HTTP://WWW.PYTHON.ORG/doc/#frag";
	auto p = urlSplit(url);
	assertEqual(p.scheme, "http");
	assertEqual(p.netloc, "WWW.PYTHON.ORG");
	assertEqual(p.path, "/doc/");
	assertEqual(p.query, "");
	assertEqual(p.fragment, "frag");
	assertEqual(p.username, null);
	assertEqual(p.password, null);
	assertEqual(p.hostname, "www.python.org");
	assert (p.port.isNull());
	// getUrl() won't return exactly the original URL in this case
	// since the scheme is always case-normalized
	// We handle this by ignoring the first 4 characters of the URL
	assertEqual(p.getUrl()[4..$], url[4..$]);

	url = "http://User:Pass@www.python.org:080/doc/?query=yes#frag";
	p = urlSplit(url);
	assertEqual(p.scheme, "http");
	assertEqual(p.netloc, "User:Pass@www.python.org:080");
	assertEqual(p.path, "/doc/");
	assertEqual(p.query, "query=yes");
	assertEqual(p.fragment, "frag");
	assertEqual(p.username, "User");
	assertEqual(p.password, "Pass");
	assertEqual(p.hostname, "www.python.org");
	assertEqual(p.port.get(), 80);
	assertEqual(p.getUrl(), url);

	// Addressing issue1698, which suggests Username can contain
	// "@" characters.  Though not RFC compliant, many ftp sites allow
	// and request email addresses as usernames.

	url = "http://User@example.com:Pass@www.python.org:080/doc/?query=yes#frag";
	p = urlSplit(url);
	assertEqual(p.scheme, "http");
	assertEqual(p.netloc, "User@example.com:Pass@www.python.org:080");
	assertEqual(p.path, "/doc/");
	assertEqual(p.query, "query=yes");
	assertEqual(p.fragment, "frag");
	assertEqual(p.username, "User@example.com");
	assertEqual(p.password, "Pass");
	assertEqual(p.hostname, "www.python.org");
	assertEqual(p.port.get(), 80);
	assertEqual(p.getUrl(), url);

	// Verify an illegal port raises ValueError
	url = "HTTP://WWW.PYTHON.ORG:65536/doc/#frag";
	p = urlSplit(url);
	assertThrown(p.port);
}

unittest
{
    // test_attributes_bad_port
	// Check handling of invalid ports.
	const ports = ["foo", "1.5", "-1", "0x10"];

	foreach (port; ports)
	{
		auto netloc = "www.example.net:" ~ port;
		auto url = "http://" ~ netloc;
		auto p = urlParse(url);
		assertEqual(p.netloc, netloc);
		assertThrown(p.port, "Failed to detect invalid port when parsing URL " ~ url);
		auto s = urlSplit(url);
		assertEqual(s.netloc, netloc);
		assertThrown(s.port, "Failed to detect invalid port when splitting URL " ~ url);
	}
}

unittest
{
	// test_attributes_without_netloc
	// This example is straight from RFC 3261.  It looks like it
	// should allow the username, hostname, and port to be filled
	// in, but doesn't.  Since it's a URI and doesn't use the
	// scheme://netloc syntax, the netloc and related attributes
	// should be left empty.
	auto uri = "sip:alice@atlanta.com;maddr=239.255.255.1;ttl=15";
	auto s = urlSplit(uri);
	assertEqual(s.netloc, "");
	assertEqual(s.username, null);
	assertEqual(s.password, null);
	assertEqual(s.hostname, null);
	assert (s.port.isNull());
	assertEqual(s.getUrl(), uri);

	auto p = urlParse(uri);
	assertEqual(p.netloc, "");
	assertEqual(p.username, null);
	assertEqual(p.password, null);
	assertEqual(p.hostname, null);
	assert (p.port.isNull());
	assertEqual(p.getUrl(), uri);
}

unittest
{
    // test_noslash
	// Issue 1637: http://foo.com?query is legal
	assertEqual(urlParse("http://example.com?blahblah=/foo"),
			URLParseResult("http", "example.com", "", "", "blahblah=/foo", ""));
}

unittest
{
	// test_withoutscheme
	// Test urlParse without scheme
	// Issue 754016: urlParse goes wrong with IP:port without scheme
	// RFC 1808 specifies that netloc should start with //, urlParse expects
	// the same, otherwise it classifies the portion of url as path.
	assertEqual(urlParse("path"), URLParseResult("","","path","","",""));
	assertEqual(urlParse("//www.python.org:80"), URLParseResult("","www.python.org:80","","","",""));
	assertEqual(urlParse("http://www.python.org:80"), URLParseResult("http","www.python.org:80","","","",""));
}

unittest
{
	// test_portseparator
	// Issue 754016 makes changes for port separator ":" from scheme separator
	assertEqual(urlParse("http:80"), URLParseResult("http","","80","","",""));
	assertEqual(urlParse("https:80"), URLParseResult("https","","80","","",""));
	assertEqual(urlParse("path:80"), URLParseResult("path","","80","","",""));
	assertEqual(urlParse("http:"), URLParseResult("http","","","","",""));
	assertEqual(urlParse("https:"), URLParseResult("https","","","","",""));
	assertEqual(urlParse("http://www.python.org:80"),
		URLParseResult("http","www.python.org:80","","","",""));
}

unittest
{
	// test_anyscheme
	// Issue 7904: s3://foo.com/stuff has netloc "foo.com".
	assertEqual(urlParse("s3://foo.com/stuff"),
		URLParseResult("s3", "foo.com", "/stuff", "", "", ""));
	assertEqual(urlParse("x-newscheme://foo.com/stuff"),
		URLParseResult("x-newscheme", "foo.com", "/stuff", "", "", ""));
	assertEqual(urlParse("x-newscheme://foo.com/stuff?query#fragment"),
		URLParseResult("x-newscheme", "foo.com", "/stuff", "", "query", "fragment"));
	assertEqual(urlParse("x-newscheme://foo.com/stuff?query"),
		URLParseResult("x-newscheme", "foo.com", "/stuff", "", "query", ""));
}

unittest
{
    // test_default_scheme
	{
		// Exercise the scheme parameter of urlParse()
		auto result = urlParse("http://example.net/", "ftp");
		assertEqual(result.scheme, "http");
		assertEqual(urlParse("path", "ftp").scheme, "ftp");
		assertEqual(urlParse("path").scheme, "");
	}
	{
		// and urlSplit()
		auto result = urlSplit("http://example.net/", "ftp");
		assertEqual(result.scheme, "http");
		assertEqual(urlSplit("path", "ftp").scheme, "ftp");
		assertEqual(urlSplit("path").scheme, "");
	}
}

unittest
{
	// test_parse_fragments
	// Exercise the allow_fragments parameter of urlParse() and urlSplit()
	static struct TestCase
	{
		string url, attr, expected_frag;
	}
	auto test_cases = [
		TestCase("http:#frag", "path", "frag"),
		TestCase("//example.net#frag", "path", "frag"),
		TestCase("index.html#frag", "path", "frag"),
		TestCase(";a=b#frag", "params", "frag"),
		TestCase("?a=b#frag", "query", "frag"),
		TestCase("#frag", "path", "frag"),
		TestCase("abc#@frag", "path", "@frag"),
		TestCase("//abc#@frag", "path", "@frag"),
		TestCase("//abc:80#@frag", "path", "@frag"),
		TestCase("//abc#@frag:80", "path", "@frag:80"),
	];

	string getAttrValue(V)(ref V v, string attr)
	{
		switch (attr)
		{
			case "params":
				static if (is(V == URLParseResult))
				{
					return v.params;
				}
				else
				{
					return v.path;
				}
			case "query":
				return v.query;
			case "path":
				return v.path;
			default:
				assert (false);
		}
	}
	foreach (uat; test_cases)
	{
		auto p = urlParse(uat.url, "", No.allowFragments);
		assertEqual(p.fragment, "");
		string attr_value = getAttrValue(p, uat.attr);
		assert (attr_value.endsWith("#" ~ uat.expected_frag), attr_value ~ " expected to be #" ~ uat.expected_frag);
		p = urlParse(uat.url, "", Yes.allowFragments);
		assertEqual(p.fragment, uat.expected_frag);
		assert (!getAttrValue(p, uat.attr).endsWith(uat.expected_frag));

		auto s = urlSplit(uat.url, "", No.allowFragments);
		assertEqual(s.fragment, "");
		attr_value = getAttrValue(s, uat.attr);
		assert (attr_value.endsWith("#" ~ uat.expected_frag), attr_value ~ " expected to be #" ~ uat.expected_frag);
		s = urlParse(uat.url, "", Yes.allowFragments);
		assertEqual(s.fragment, uat.expected_frag);
		assert (!getAttrValue(s, uat.attr).endsWith(uat.expected_frag));
	}
}

unittest
{
	// test_parse_qs_encoding
	// NB: "latin-1" isn't recognised as an alias of "iso-8859-1" by the D standard library as of writing
	auto result = parseQS("key=\u0141%E9", No.keepBlankValues, Yes.strictParsing, "iso-8859-1");
	assertEqual(result, ["key": ["\u0141\u00e9"]], "1");
	result = parseQS("key=\u0141%C3%A9", No.keepBlankValues, Yes.strictParsing, "utf-8");
	assertEqual(result, ["key": ["\u0141\u00e9"]], "2");
	result = parseQS("key=\u0141%C3%A9", No.keepBlankValues, Yes.strictParsing, "ascii");
	assertEqual(result, ["key": ["\u0141\ufffd\ufffd"]], "3");
	result = parseQS("key=\u0141%E9-", No.keepBlankValues, Yes.strictParsing, "ascii");
	assertEqual(result, ["key": ["\u0141\ufffd-"]], "4");
	result = parseQS("key=\u0141%E9-", No.keepBlankValues, Yes.strictParsing, "ascii", EncodingErrorHandling.ignore);
	assertEqual(result, ["key": ["\u0141-"]], "5");
}

unittest
{
    // test_parse_qsl_max_num_fields
	assertThrown(parseQS("a=a".repeat(11).join('&'), No.keepBlankValues, No.strictParsing, "utf-8", EncodingErrorHandling.replace, 10),
		"Failed to detect too many &-separated fields");

	assertThrown(parseQS("a=a".repeat(11).join(';'), No.keepBlankValues, No.strictParsing, "utf-8", EncodingErrorHandling.replace, 10),
		"Failed to detect too many ;-separated fields");

	// Just inside limit
	parseQS("a=a".repeat(10).join(';'), No.keepBlankValues, No.strictParsing, "utf-8", EncodingErrorHandling.replace, 10);
}

unittest
{
	// test_urlencode_quote_via
	auto result = urlEncode(["a": "some value"]);
	assertEqual(result, "a=some+value");
	result = urlEncode(["a": "some value/another"],
			"", "utf-8", EncodingErrorHandling.ignore, &quote);
	assertEqual(result, "a=some%20value%2Fanother");
	result = urlEncode(["a": "some value/another"],
			"/", "utf-8", EncodingErrorHandling.ignore, &quote);
	assertEqual(result, "a=some%20value/another");
}

unittest
{
	// test_quote_from_bytes
	auto result = quoteFromBytes("archaeological arcana".representation);
	assertEqual(result, "archaeological%20arcana");
	result = quoteFromBytes("".representation);
	assertEqual(result, "");
}

unittest
{
    // test_unquote_to_bytes
	auto result = unquoteToBytes("abc%20def");
	assertEqual(result, "abc def".representation);
	result = unquoteToBytes("");
	assertEqual(result, "".representation);
}

unittest
{
	// test_issue14072
	auto s1 = urlSplit("tel:+31-641044153");
	assertEqual(s1.scheme, "tel");
	assertEqual(s1.path, "+31-641044153");
	auto s2 = urlSplit("tel:+31641044153");
	assertEqual(s2.scheme, "tel");
	assertEqual(s2.path, "+31641044153");
	// assert the behavior for urlparse
	auto p1 = urlParse("tel:+31-641044153");
	assertEqual(p1.scheme, "tel");
	assertEqual(p1.path, "+31-641044153");
	auto p2 = urlParse("tel:+31641044153");
	assertEqual(p2.scheme, "tel");
	assertEqual(p2.path, "+31641044153");
}

unittest
{
    // test_port_casting_failure_message
	auto message = "Port could not be cast to integer value as 'oracle'";
	auto p1 = urlParse("http://Server=sde; Service=sde:oracle");
	auto ex_msg = collectExceptionMsg!ValueException(p1.port);
	assertEqual(ex_msg, message);

	auto p2 = urlSplit("http://Server=sde; Service=sde:oracle");
	ex_msg = collectExceptionMsg!ValueException(p2.port);
	assertEqual(ex_msg, message);
}

unittest
{
	// test_telurl_params
	auto p1 = urlParse("tel:123-4;phone-context=+1-650-516");
	assertEqual(p1.scheme, "tel");
	assertEqual(p1.path, "123-4");
	assertEqual(p1.params, "phone-context=+1-650-516");

	p1 = urlParse("tel:+1-201-555-0123");
	assertEqual(p1.scheme, "tel");
	assertEqual(p1.path, "+1-201-555-0123");
	assertEqual(p1.params, "");

	p1 = urlParse("tel:7042;phone-context=example.com");
	assertEqual(p1.scheme, "tel");
	assertEqual(p1.path, "7042");
	assertEqual(p1.params, "phone-context=example.com");

	p1 = urlParse("tel:863-1234;phone-context=+1-914-555");
	assertEqual(p1.scheme, "tel");
	assertEqual(p1.path, "863-1234");
	assertEqual(p1.params, "phone-context=+1-914-555");
}

unittest
{
    // test_urlsplit_normalization
	// Certain characters should never occur in the netloc,
	// including under normalization.
	// Ensure that ALL of them are detected and cause an error

	// bpo-36742: Verify port separators are ignored when they
	// existed prior to decomposition
	urlSplit("http://\u30d5\u309a:80");
	assertThrown(urlSplit("http://\u30d5\u309a\ufe1380"));

	const illegal_chars = "/:#?@"d;
	foreach (c; unicode.Any.byCodepoint)
	{
		if (isASCII(c)) continue;
		auto decomp = decompose!Compatibility(c);
		auto is_dangerous = decomp[].any!(cp => illegal_chars.canFind(cp));
		if (is_dangerous)
		{
			foreach (scheme; ["http", "https", "ftp"])
			{
				foreach (netloc; ["netloc%cfalse.netloc", "n%cuser@netloc"])
				{
					auto url = scheme ~ "://" ~ format(netloc, c) ~ "/path";
					assertThrown(urlSplit(url), format("%c not properly normalized", c));
				}
			}
		}
	}
}

unittest
{
	// encodeString
	static struct TestCase
	{
		string input;
		string encoding;
		EncodingErrorHandling errors;
		immutable(ubyte)[] expected;
	}

	with(EncodingErrorHandling)
	{
		auto test_cases = [
			TestCase("", "utf-8", strict, "".representation),
			TestCase("asdf", "utf-8", strict, "asdf".representation),
			TestCase("asdf", "ascii", strict, "asdf".representation),
			TestCase("asdf", "iso-8859-1", strict, "asdf".representation),
			TestCase("asdf！", "ascii", ignore, "asdf".representation),
			TestCase("asdf！", "ascii", replace, "asdf?".representation),
			TestCase("！", "utf-8", strict, "！".representation),
			TestCase("！", "ascii", ignore, "".representation),
			TestCase("！", "ascii", replace, "?".representation),
			TestCase("😀", "ascii", replace, "?".representation),
			TestCase("😀", "utf-8", strict, "😀".representation),
			TestCase("！asdf", "ascii", ignore, "asdf".representation),
			TestCase("！as！df！", "ascii", ignore, "asdf".representation),
			TestCase("！asdf", "ascii", replace, "?asdf".representation),
			// e with combining acute
			TestCase("e\u0301", "ascii", ignore, "".representation),
			TestCase("e\u0301", "iso-8859-1", ignore, [0xe9]),

			TestCase("caractères codés", "iso-8859-1", strict, "caract\xe8res cod\xe9s".representation),
		];

		foreach (uat; test_cases)
		{
			auto result = encodeString(uat.input, uat.encoding, uat.errors);
			assertEqual(result, uat.expected, format("Error testing encodeString on %s", uat));
		}
	}
}

unittest
{
	// decodeBytes
	static struct TestCase
	{
		immutable(ubyte)[] input;
		string encoding;
		EncodingErrorHandling errors;
		string expected;
	}

	with(EncodingErrorHandling)
	{
		auto test_cases = [
			TestCase("".representation, "utf-8", strict, ""),
			TestCase("asdf".representation, "utf-8", strict, "asdf"),
			TestCase("asdf".representation, "ascii", strict, "asdf"),
			TestCase("asdf".representation, "iso-8859-1", strict, "asdf"),
			TestCase("asdf！".representation, "ascii", ignore, "asdf"),
			TestCase("asdf！".representation, "ascii", replace, "asdf\ufffd\ufffd\ufffd"),
			TestCase("！".representation, "utf-8", strict, "！"),
			TestCase("！".representation, "ascii", ignore, ""),
			TestCase("！".representation, "ascii", replace, "\ufffd\ufffd\ufffd"),
			TestCase("😀".representation, "ascii", replace, "\ufffd\ufffd\ufffd\ufffd"),
			TestCase("😀".representation, "utf-8", strict, "😀"),
			TestCase("！asdf".representation, "ascii", ignore, "asdf"),
			TestCase("！as！df！".representation, "ascii", ignore, "asdf"),
			TestCase("！asdf".representation, "ascii", replace, "\ufffd\ufffd\ufffdasdf"),
			// e with combining acute
			TestCase("e\u0301".representation, "ascii", ignore, "e"),

			TestCase("caract\xe8res cod\xe9s".representation, "iso-8859-1", strict, "caractères codés"),
			TestCase("br\xff\xff\xff\xffken text！\xff".representation, "utf-8", replace, "br\ufffd\ufffd\ufffd\ufffdken text！\ufffd"),
		];

		foreach (uat; test_cases)
		{
			auto result = decodeBytes(uat.input, uat.encoding, uat.errors);
			assertEqual(result, uat.expected, format("Error testing decodeBytes on %s", uat));
		}
	}
}
